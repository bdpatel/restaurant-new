package com.example.bhavik.restaurant;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class ConformationActivity extends AppCompatActivity {
    Button homepage;
    TextView confirmation;
   // TextView textdate,texttime,textnop;
   // TextView tableCount;

    public void nextpagemethod() {
        homepage = findViewById(R.id.btnhome);
        homepage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homepg = new Intent(ConformationActivity.this,MainActivity.class);
                startActivity(homepg);
            }
        });
    }

    /*public ConformationActivity(TextView table) {
        this.tableCount = table;

    }

    public ConformationActivity(TextView date, TextView time,TextView nop) {
        this.textdate = date;
        this.texttime = time;
        this.textnop = nop;
    }
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conformation);
        //confirmation = findViewById(R.id.txtcp);

        //confirmation.setText(tableCount.getText());

        //confirmation.setText(textdate.getText());
       // confirmation.setText(texttime.getText());
        //confirmation.setText(textnop.getText());
        confirmation = findViewById(R.id.txtcp);

        confirmation.setText(confirmation.getText()+"Reservation Confirmed at Spicy India Restaurant \n");
        nextpagemethod();


    }
}
