package com.example.bhavik.restaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class ScreenView extends AppCompatActivity {
    CheckBox checkbox1,checkbox2,checkbox3,checkbox4;
    TextView display;

    Button nextpage;

    public void nextpagemethod() {

        nextpage = findViewById(R.id.btnnextpage);
        nextpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new ConformationActivity(display);
                Intent nxt = new Intent(ScreenView.this,ConformationActivity.class);
                startActivity(nxt);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_view);
        checkbox1 = findViewById(R.id.cb1);
        checkbox2 = findViewById(R.id.cb2);
        checkbox3 = findViewById(R.id.cb3);
        checkbox4 = findViewById(R.id.cb4);

        display =findViewById(R.id.DisplaytextView);

        checkbox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
                display.setText(display.getText()+"Table for 2 selected");
            }
        });
        checkbox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
                display.setText(display.getText()+"Table for 4 selected");
            }
        });
        checkbox3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
                display.setText(display.getText()+"Table for 6 selected");
            }
        });
        checkbox4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText("");
                display.setText(display.getText()+"Table for 8 selected");
            }
        });

        nextpagemethod();

    }
}
