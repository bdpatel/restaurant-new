package com.example.bhavik.restaurant;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    private Button btnMoreOption;
    private TextView uname,pass;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnMoreOption = findViewById(R.id.btnloginsuccess);
        uname = findViewById(R.id.txtuname);
        pass = findViewById(R.id.txtpass);
        mAuth =  FirebaseAuth.getInstance();

        btnMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                userLogin();
            }
        });
    }



    private void userLogin()
    {
        String email = uname.getText().toString();
        String password = pass.getText().toString();

        if(TextUtils.isEmpty(email))
        {
            Toast.makeText(this, "Please fill up your email fields", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(password))
        {
            Toast.makeText(this, "Please fill up your password fields", Toast.LENGTH_SHORT).show();
        }else
        {
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task)
                {
                    if(task.isSuccessful())
                    {
                        moreOption();
                        Toast.makeText(LoginActivity.this, "You Successfully logged-in", Toast.LENGTH_SHORT).show();

                    }else{
                        String message = task.getException().getMessage();
                        Toast.makeText(LoginActivity.this," "+message,Toast.LENGTH_SHORT);
                    }
                }
            });
        }
    }

    private void moreOption() {

        btnMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(LoginActivity.this,MoreOptions.class);
                login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(login);
                finish();
            }
        });
    }


}
