package com.example.bhavik.restaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    TextView about;
    Button  aboutpage;


    public void backtomain() {
        aboutpage = findViewById(R.id.btnabtus);
        aboutpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homepg = new Intent(AboutActivity.this,MainActivity.class);
                startActivity(homepg);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        about = findViewById(R.id.txtabt);
        about.setText(about.getText()+
                " This app is designed to shows a virtual image of the restaurant on a particular android devices," +
                " where user are able to view all the available tables at a particular restaurant.\n");
        backtomain();
    }
}
