package com.example.bhavik.restaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MoreOptions extends AppCompatActivity {
    Button btnMoreOption;
    TextView tvdt,tvtime,tvnop;
    TextView uname,pass;
    TextView status,role;

    public void screenview() {
        btnMoreOption = findViewById(R.id.btnnextpage);
        btnMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent next = new Intent(MoreOptions.this,ScreenView.class);
                startActivity(next);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_options);
        tvdt = findViewById(R.id.txtdt);
        tvtime = findViewById(R.id.txttime);
        tvnop = findViewById(R.id.txtnop);

        status = findViewById(R.id.txtstatus);
        role = findViewById(R.id.txtrole);

        //new ConformationActivity(this,status,role,0,tvdt,tvtime,tvnop);
        screenview();
    }

}
